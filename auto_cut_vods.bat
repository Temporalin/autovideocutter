@ECHO off
FOR /F "usebackq delims=, tokens=1,2,3,4*" %%a in (`type %1`) do (
    ffmpeg -i Fastfall.mp4 -ss %%b -t %%c -async 1 "%%a.mp4"
)
pause