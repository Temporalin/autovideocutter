# AutoVideoCutter

Script para cortar vídeos con **fmpeg** automáticamente a partir de un *.csv*

# Cómo usarlo

- Pon el archivo *.bat* en la carpeta **bin** de **fmpeg**.
- Guarda un archivo *.csv* como el del ejemplo en la misma carpeta.
- Copia o mueve el archivo a cortar a la misma carpeta.
- **Edita el *.bat* y cambia el nombre del archivo de entrada**. Es decir, cambia *Fastfall.mp4* por el nombre de tu archivo.
- **Alternativa al paso anterior:** llama al archivo a cortar *Fastfall.mp4*
- Arrastra el archivo *.csv* sobre el *.bat*

# Formato del CSV

Son 3 columnas: el nombre de la partida, el momento en el que empieza y la duración.

Los tiempos han de ir en formato **hh:mm:ss**

# ¿Dudas, sugerencias?

Twitter: [@HeroOfTemp](https://twitter.com/HeroOfTemp)